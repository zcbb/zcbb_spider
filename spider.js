﻿var fs = require('fs');
var http = require('http');
var nedb = require('nedb');
var cheerio = require('cheerio');
var request = require('request');
var iconv = require('iconv-lite')

_json = JSON.parse(fs.readFileSync('./HolyBible.json'))


function fetchPage() {
    for (i in _json['Juan']) {
        _x = _json['Juan'][i]
        if (_x in _json) {
            var db = new nedb({
                filename: './db/' + _json[_x]['filename'] +'.db',
                autoload: true
                })
            _juan_ch = _json[_x]['ch-name']
            _juan_en = _json[_x]['en-name']
            _urls = _json['url']
            _start = _json[_x]['start']
            _end = _json[_x]['end']
            _num = _json[_x]['juan']
            for (var k = _start; k < _end; k++) {
                _u = []
                for (i in _urls) {
                    _u1 = _urls[i].replace('juan', _num)
                    _u2 = _u1.replace('zhang', k)
                    _u.push(_u2)
                }
                startRequest(db, _u, _juan_ch, _juan_en, k)
            }
        } else {
            console.log(_x + ' :暂未维护,敬请期待...')
        }
    }
}

function startRequest(db, urls, juan_ch, juan_en, zhang) {
    _r = []
    _dist1 = {}
    sp_cn(db, urls[0], juan_ch, juan_en, zhang)
    sp_en(db, urls[1], juan_en, zhang)
}

function sp_cn(db, url, juan_ch, juan_en, zhang) {
    console.log('爬取url:' + url)
    http.get(url, function (res) {
        var chunks = []       
        res.on('data', function (chunk) {   
            chunks.push(chunk);
        })
        res.on('end', function () {
            var html = iconv.decode(Buffer.concat(chunks), 'gb2312');
            var $ = cheerio.load(html, {decodeEntities: false});
            var holy = []
            $('font.tk4l').each(function(i, elem) {
                holy[i] = $(this).text();
                db.insert({
                    'juan_ch': juan_ch,
                    'juan_en': juan_en,
                    'zhang': zhang,
                    'jie': i+1,
                    'cvalue': holy[i]
                }, (err, ret) => {
                })
            })
        })
    })
}
function sp_en(db, url, juan_en, zhang) {
    console.log('爬取url:' + url)
    http.get(url, function (res) {
        var chunks = []       
        res.on('data', function (chunk) {   
            chunks.push(chunk);
        })
        res.on('end', function () {
            var html = iconv.decode(Buffer.concat(chunks), 'gb2312');
            var $ = cheerio.load(html, {decodeEntities: false});
            var holy = []
            $('font.tk4l').each(function(i, elem) {
                holy[i] = $(this).text();
                db.update({
                    'juan_en': juan_en,
                    'zhang': zhang,
                    'jie': i+1
                }, {
                    $set: {
                      'evalue': holy[i]
                    }
                  }, (err, ret) => {
                  })
            })
        })
    })
}
fetchPage()
